# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "admin01",
             email: "admin01@admin.com",
             password:              "123456",
             password_confirmation: "123456",
             admin: true)


doc=Nokogiri::XML(open("http://www.related.com/feeds/ZillowAvailabilities.xml"))
doc.xpath('//Location').each do |location_elements|
      
    @location = Location.new
    para={}
    para[:address]=location_elements.xpath('StreetAddress').text
      
    para[:unit]=location_elements.xpath('UnitNumber').text
    para[:city]=location_elements.xpath('City').text
    para[:state]=location_elements.xpath('State').text
    para[:zipcode]=location_elements.xpath('Zip').text
    para[:latitude]=nil
    para[:longitude]=nil
    
    i=0
    j=10
    while i<j && @location.latitude==nil do
    @location = Location.new(para)
    @location.save
    i+=1
    end
      
end