require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Practice Application"
    assert_equal full_title("Help"), "Help | Practice Application"
  end
end