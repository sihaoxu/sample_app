class LocationsController < ApplicationController
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :destroy, :show]
  

  # GET /locations
  # GET /locations.json
  def index
    @locations = Location.paginate(page: params[:page])
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
  end


  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    authorize! :destroy, @location
    @location.destroy
    flash[:success] = "destroyed apartment successfully!"
    redirect_to locations_url
    #respond_to do |format|
    #  format.html { redirect_to locations_url, notice: 'Location was successfully destroyed.' }
    #  format.json { head :no_content }
    #end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:address, :city, :state, :zipcode, :unit, :latitude, :longitude)
    end
    
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
end
