require 'test_helper'

class LocationsIndexTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @location = locations(:one)
    @admin     = users(:michael)
    @non_admin = users(:archer)
  end
  
  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get locations_path
    assert_template 'locations/index'
    
    assert_difference 'Location.count', -1 do
      delete location_path(@location)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get locations_path
    assert_select 'a', text: 'delete', count: 0
  end
  
end
