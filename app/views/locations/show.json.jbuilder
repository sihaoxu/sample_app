json.extract! @location, :id, :address, :city, :state, :zipcode, :unit, :latitude, :longitude, :created_at, :updated_at
