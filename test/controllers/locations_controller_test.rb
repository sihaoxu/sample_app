require 'test_helper'

class LocationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @location = locations(:one)
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should get index" do
    log_in_as(@user)
    get locations_url
    assert_response :success
  end


  test "should show location" do
    log_in_as(@user)
    get location_url(@location)
    assert_response :success
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Location.count' do
      delete location_path(@location)
    end
    assert_redirected_to login_url
  end  

  #test "should destroy location" do
  #  assert_difference('Location.count', -1) do
  #    delete location_url(@location)
  #  end

  #  assert_redirected_to locations_url
  #end
end
