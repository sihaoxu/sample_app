# Ruby on Rails practice application

This is the practice application developed by Sihao Xu based on 
[*Ruby on Rails: *](http://www.railstutorial.org/) 
accroding to the tutorial book [*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).
by [Michael Hartl](http://www.michaelhartl.com/).
